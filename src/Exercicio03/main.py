'''
Faça um Programa que leia 20 números inteiros e armazene-os num vetor.
Armazene os números pares no vetor PAR e os números ÍMPARES no vetor ímpar.
Imprima os três vetores.
'''

def main():
  NUM_NUMEROS = 20
  numeros_lidos = []
  numeros_pares = []
  numeros_impares = []

  for i in range(NUM_NUMEROS):
    numero = input(f"Número {i + 1}: ")

    if not numero.isnumeric():
      print('Valor não numérico. Fim do programa')
      exit(-1)

    numero = float(numero)
    
    numeros_lidos.append(numero)
    numeros_pares.append(numero) if numero % 2 == 0 else numeros_impares.append(numero)

  print('Números digitados:')
  print(numeros_lidos)
  print('Números pares digitados:')
  print(numeros_pares)
  print('Números ímpares digitados:')
  print(numeros_impares)


if __name__ == '__main__':
  main()
