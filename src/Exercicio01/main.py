'''
Faça um Programa que leia 4 notas de alunos e, ao final, mostre na tela as notas lidas e a respectiva média.
'''

def main():
  NUM_NOTAS = 4
  notas_lidas = []
  
  for i in range(NUM_NOTAS):
    nota = input(f"Nota {i + 1}: ")

    if not nota.isnumeric():
      print('Nota inválida. Fim do programa!')
      exit(-1)
    
    notas_lidas.append(float(nota))

  media = sum(notas_lidas) / NUM_NOTAS

  print('As notas lidas foram: ')
  print(notas_lidas)
  print(f'A Média das notas é {media}')


if __name__ == '__main__':
  main()