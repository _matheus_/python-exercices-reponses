'''
Faça um programa que faça 5 perguntas para uma pessoa sobre um crime. As perguntas são:
 - "Telefonou para a vítima?"
 - "Esteve no local do crime?"
 - "Mora perto da vítima?"
 - "Devia dinheiro para a vítima?"
 - "Já trabalhou com a vítima?"
No final, o programa deve emitir uma classificação sobre a participação da pessoa no crime.
Se a pessoa responder positivamente a 2 questões ela deve ser classificada como "Suspeita";
entre 3 e 4, como "Cúmplice", e 5 como "Assassino". Caso contrário, ele será classificado como "Inocente".
'''

def main():
  print('INTERROGATÓRIO\n##############\n')
  telefonou = input('Telefonou para a vítima (S|N)? ').lower() == "s"
  esteve_local = input('Esteve no local do crime (S|N)? ').lower() == "s"
  mora_perto = input('Mora perto da vítima (S|N)? ').lower() == "s"
  devia_dinheiro = input('Devia dinheiro para a vítima (S|N)? ').lower() == "s"
  trabalhou = input('Já trabalhou com a vítima (S|N)? ').lower() == "s"

  pontos = sum([telefonou, esteve_local, mora_perto, devia_dinheiro, trabalhou])

  if pontos == 5:
    print("ASSASSINO")
  elif pontos > 2:
    print("CÚMPLICE")
  elif pontos == 2:
    print("SUSPEITO")
  else:
    print("INOCENTE")


if __name__ == '__main__':
  main()
