'''
Um palíndromo é uma sequência de caracteres cuja leitura é idêntica se feita da direita para esquerda ou
vice−versa. Por exemplo: OSSO e OVO são palíndromos. Em textos mais complexos os espaços e pontuação
são ignorados. A frase SUBI NO ONIBUS é o exemplo de uma frase palíndroma onde os espaços foram
ignorados. Faça um programa que leia uma sequência de caracteres e diga se esta é um palíndromo ou não.
'''


def main():
    frase = input('Informe uma palavra ou frase: ')
    pontuacoes = ['.', ',', ' ']
    for p in pontuacoes:
        frase = frase.replace(p, '')

    nova_frase = []
    nova_frase[:0] = frase
    nova_frase.reverse()
    nova_frase = ''.join(nova_frase)

    if frase == nova_frase:
        print('É palíndromo')
    else:
        print('Não é palíndromo')


if __name__ == '__main__':
    main()
