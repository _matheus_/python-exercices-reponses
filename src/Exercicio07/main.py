'''
Crie uma classe para representar datas com as seguintes regras:
 - deve ter três atributos: o dia, o mês e o ano;
 - deve ter um construtor que inicializa os três atributos e verifica a validade dos valores fornecidos;
 - encapsule cada um dos atributos;
 - forneça o método __str__ para retornar uma representação da data como string.
   Considere que a data deve ser formatada mostrando o dia, o mês e o ano separados por barra (/);
 - forneça uma operação para avançar uma data para o dia seguinte.
'''

from lib import Data

def main():
  data = Data(dia=31, mes=12, ano=2021)
  print(f'Data criada: {data}')
  data.avancar()
  print(f'Data avançada para o dia seguinte: {data}')
  

if __name__ == '__main__':
  main()
