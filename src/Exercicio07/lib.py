class Data:
  __MESES_30_DIAS = [4, 6, 9, 11]

  def __init__(self, dia, mes, ano):
    if not self.__validar_data(dia, mes, ano):
      raise Exception('Data inválida.')

    self.__dia = dia
    self.__mes = mes
    self.__ano = ano


  @property
  def dia(self):
    return self.__dia

  @property
  def mes(self):
    return self.__mes

  @property
  def ano(self):
    return self.__ano


  def avancar(self):
    if self.__mes in self.__MESES_30_DIAS: # meses de 30 dias
      if self.__dia != 30:
        self.__dia += 1
      else:
        self.__dia = 1
        self.__mes += 1
    elif self.__mes == 2: # mês de fevereiro
      if self.__dia not in [28, 29]:
        self.__dia += 1
      else:
        if self.__dia == 28 and self.__ano_bissexto(self.__ano):
          self.__dia += 1
        else:
          self.__dia = 1
          self.__mes += 1
    else: # meses de 30 dias
      if self.__dia != 31:
        self.__dia += 1
      else:
        self.__dia = 1
        if self.__mes == 12:
          self.__mes = 1
          self.__ano += 1
        else:
          self.__mes += 1
        


  def __validar_data(self, dia, mes, ano):
    # Limites gerais
    if dia < 1 or dia > 31 or mes < 1 or mes > 12 or ano < 1:
      return False

    # Meses com 30 dias
    if mes in self.__MESES_30_DIAS and dia > 30:
      return False
    
    # Fevereiro
    bissexto = self.__ano_bissexto(ano)
    if (mes == 2) and ((bissexto and dia > 29) or (not bissexto and dia > 28)):
      return False

    return True

  
  def __ano_bissexto(self, ano):
    return (ano % 4 == 0 and ano % 100 != 0) or ano % 400 == 0


  def __str__(self):
    data = f'{"0" if self.__dia < 10 else ""}{self.__dia}/{"0" if self.__mes < 10 else ""}{self.__mes}/'
    
    if self.__ano < 100:
      if self.__ano > 29:
        data +=  f'19{self.__ano}'
      else:
        data += f'20{self.__ano}'
    else:
      data += f'{self.__ano}'
    
    return data



def ler_numero(texto, integer=True):
  valor = input(texto)

  try:
    valor = int(valor) if integer else float(valor)
    ok = True
  except:
    ok = False

  return valor if ok else None
