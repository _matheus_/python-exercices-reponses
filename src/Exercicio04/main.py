'''
Faça um Programa que peça duas notas de 5 alunos, calcule e armazene num vetor a 
média de cada aluno, imprima o número de alunos com média maior ou igual a 7.0.
'''

from lib import Aluno, ler_notas

def main():
  NUM_ALUNOS = 5
  medias = []

  for i in range(NUM_ALUNOS):
    print(f'Aluno {i + 1}:')
    notas = ler_notas()

    if notas[0] in [-1, -2]:
      print(f"Nota inválida. Fim do programa (Erro: {notas[0]}).")
      exit(-1)

    aluno = Aluno(notas)
    medias.append(aluno.media())
  
  print('Médias dos alunos:')
  print(medias)

  medias_maior_igual_sete = sum([1 if media >= 7 else 0 for media in medias])

  print(f'Médias maiores ou iguais a 7.0: {medias_maior_igual_sete}')


if __name__ == '__main__':
  main()
