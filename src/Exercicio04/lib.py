class Aluno:
  def __init__(self, notas):
    self.nota1 = notas[0]
    self.nota2 = notas[1]
  
  def media(self):
    return (self.nota1 + self.nota2) / 2


def ler_notas():
  texto = input('Notas (separadas por \'/\'. Ex. 2.9 / 5.7): ')
  notas = texto.split('/')

  if len(notas) != 2:
    return (-2,)

  if not (notas[0].strip().isnumeric() and notas[1].strip().isnumeric()):
    return (-1,)

  return float(notas[0]), float(notas[1])
