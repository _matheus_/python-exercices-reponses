'''
Implemente a classe Funcionário com um nome e um salário. Escreva um construtor com
dois parâmetros (nome e salário) e métodos para devolver nome e salário. Crie o método
aumentar_salario(percentual_aumento) que aumente o salário do funcionário em uma certa
porcentagem. Escreva um pequeno programa que teste sua classe.
'''

from lib import Funcionario

def main():
  func = Funcionario(nome='João', salario=1000)
  print(f'{func.nome} tem salário de R${func.salario}')

  func.aumentar_salario(10)

  print(f'Foi concedido aumento de 10% para {func.nome}. Seu novo salário é R${func.salario}')


if __name__ == '__main__':
  main()