class Funcionario:
  def __init__(self, nome, salario):
    self.__nome = nome
    self.__salario = salario

  @property
  def nome(self):
    return self.__nome

  @property
  def salario(self):
    return self.__salario

  def aumentar_salario(self, percentual_aumento):
    self.__salario *= 1 + (percentual_aumento / 100)