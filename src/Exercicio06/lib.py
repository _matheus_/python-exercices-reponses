MESES = {
  1: "Janeiro",
  2: "Fevereiro",
  3: "Março",
  4: "Abril",
  5: "Maio",
  6: "Junho",
  7: "Julho",
  8: "Agosto",
  9: "Setembro",
  10: "Outubro",
  11: "Novembro",
  12: "Dezembro"
}


def ler_numero(texto, integer=True):
  valor = input(texto)

  try:
    valor = int(valor) if integer else float(valor)
    ok = True
  except:
    ok = False

  return valor if ok else None
