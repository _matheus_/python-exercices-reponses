'''
Faça um programa que receba a temperatura média de cada mês do ano e armazene-as em uma lista.
Após isto, calcule a média anual das temperaturas e mostre todas as temperaturas acima da média anual,
e em que mês elas ocorreram (mostrar o mês por extenso: 1 – Janeiro, 2 – Fevereiro, . . . ).
'''
from lib import ler_numero, MESES


def main():
  temperaturas = []

  for i in MESES:
    temperatura = ler_numero(f"Temperatura média do mês de {MESES[i].lower()}: ")

    if temperatura is None:
      print("Temperatura inválida. Fim do programa.")
      exit(-1)

    temperaturas.append(temperatura)
  
  temperatura_media = sum(temperaturas)/len(MESES)

  print(f'\nTemperatura média anual: {temperatura_media}')
  print('Meses com a temperatura média acima da média anual: ')
  
  for indice, temp in enumerate(temperaturas):
    if temp > temperatura_media:
      print(f'\t{MESES[indice + 1]} ({temp})')


if __name__ == '__main__':
  main()
