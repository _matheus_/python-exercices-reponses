def ler_numero(texto, integer=True):
  valor = input(texto)

  try:
    valor = int(valor) if integer else float(valor)
    ok = True
  except:
    ok = False

  return valor if ok else None
