'''
Faça um programa que leia um número indeterminado de valores, correspondentes a notas, 
encerrando a entrada de dados quando for informado um valor igual a -1
(que não deve ser armazenado). Após esta entrada de dados, faça:
 - mostre a quantidade de valores que foram lidos;
 - exiba todos os valores na ordem em que foram informados, um ao lado do outro;
 - exiba todos os valores na ordem inversa à que foram informados, um abaixo do outro;
 - calcule e mostre a soma dos valores;
 - calcule e mostre a média dos valores;
 - calcule e mostre a quantidade de valores acima da média calculada;
 - calcule e mostre a quantidade de valores abaixo de sete;
 - encerre o programa com uma mensagem.
'''

from lib import ler_numero


def main():
  notas = []
  nota = 0

  while nota not in [None, -1]:
    nota = ler_numero('Digite um número: ', integer=False)

    if nota in [None, -1]:
      continue

    notas.append(nota)

  qtd = len(notas)
  soma = sum(notas)
  media = soma / qtd

  print('\n=====================')
  print('Relatório do programa\n=====================\n')
  print(f'Quantidade de valores lidos: {qtd}')
  print(f'Valores lidos: {notas}')
  print(f'Valores lidos (em ordem reversa):')
  [print(nota) for nota in reversed(notas)]
  print(f'Soma dos valores: {soma}')
  print(f'Média dos valores: {media}')
  print(f'Quantidade de valores acima da média: {sum([1 if nota > media else 0 for nota in notas])}')
  print(f'Quantidade de valores abaixo de 7.0: {sum([1 if nota < 7 else 0 for nota in notas])}')

  print('\nFim do programa.\n')


if __name__ == '__main__':
  main()
