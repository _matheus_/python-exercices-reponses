'''
Numa eleição existem três candidatos. Faça um programa que peça o número total de eleitores. Peça para cada
eleitor votar e ao final mostrar o número de votos de cada candidato.
'''


def encerrar_programa_com_erro(mensagem):
    print(f'{mensagem} Fim do programa.')
    exit(-1)


def main():
    canditatos = {
        'A': 0, 'B': 0, 'C': 0
    }

    try:
        total_eleitores = int(input('Informe a quantidade de eleitores: '))
    except:
        encerrar_programa_com_erro('Número inválido.')

    for i in range(total_eleitores):
        print('Informe seu voto:')
        voto = input('> Entre com o código do cadidato: A, B ou C: ').upper()

        if voto not in canditatos:
            encerrar_programa_com_erro('Este candidato não existe.')

        canditatos[voto] += 1

    print('Contagem de votos: ')
    for c in canditatos:
        print(f'Candidato {c} ({canditatos[c]})')


if __name__ == '__main__':
    main()
