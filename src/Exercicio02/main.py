'''
Faça um Programa que leia uma string e diga quantas consoantes foram lidas. Imprima as consoantes.
'''

def main():
  CONSOANTES = 'bcdfghjklmnpqrstvwxyz'
  consoantes_lidas = []

  palavra = input('Digite uma palavra: ')

  for caracter in palavra:
    if CONSOANTES.find(caracter.lower()) > -1:
      consoantes_lidas.append(caracter)
  
  print(f'Foram lidas {len(consoantes_lidas)} consoantes na palavra {palavra}.')
  print('As consoantes são:')
  print(consoantes_lidas)


if __name__ == '__main__':
  main()
