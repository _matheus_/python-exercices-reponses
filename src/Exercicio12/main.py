'''
Faça um programa que solicite a data de nascimento (dd/mm/aaaa) do usuário e imprima a data com o nome do mês por extenso.
Ex: Data de Nascimento: 29/10/1973
      Você nasceu em  29 de Outubro de 1973.
'''

from dateutil import parser
from lib import MESES

def main():
  data_nascimento = input('Data de nascimento (dd/mm/aaaa): ')

  data_nascimento = parser.parse(data_nascimento, dayfirst=True)

  print(f'Você nasceu em {data_nascimento.day} de {MESES[data_nascimento.month]} de {data_nascimento.year}')


if __name__ == '__main__':
  main()
