'''
Faça um programa completo utilizando classes e métodos que:
 - Possua uma classe chamada bombaCombustível, com no mínimo esses atributos:
     - tipoCombustivel.
     - valorLitro
     - quantidadeCombustivel
 - Possua no mínimo esses métodos:
     - abastecerPorValor( ) – método onde é informado o valor a ser abastecido e mostra a quantidade de litros que foi colocada no veículo
     - abastecerPorLitro( ) – método onde é informado a quantidade em litros de combustível e mostra o valor a ser pago pelo cliente.
     - alterarValor( ) – altera o valor do litro do combustível.
     - alterarCombustivel( ) – altera o tipo do combustível.
     - alterarQuantidadeCombustivel( ) – altera a quantidade de combustível restante na bomba.
OBS: Sempre que acontecer um abastecimento é necessário atualizar a quantidade de combustível total na bomba.
'''

from posto import BombaCombustivel
import tela


def main():
  bomba = BombaCombustivel(valor_litro=7.89, quantidade_combustivel=1000)
  opcao = -1

  while opcao != 0:
    tela.menu(tipo_combustivel=bomba.tipo_combustivel, valor_litro=bomba.valor_litro,
                                quantidade_combustivel=bomba.quantidade_combustivel)

    opcao = tela.ler_numero('\nOpção: ')

    if opcao in [None, -1]:
      print('Informe um número no formato válido.')

    elif opcao == 0:
      print('Fim do programa!')

    # Abastecer veículo por litro
    elif opcao == 1:
      litros = tela.ler_numero("Litros a abastecer: ", integer=False)

      try:
        valor_pagar = bomba.abastecer_por_litro(litros)
        print(f'Abastecimento completo. Você deve R$ {valor_pagar:.2f}')
      except Exception as ex:
        print(ex)

    # Abastecer veículo por valor
    elif opcao == 2:
      valor = tela.ler_numero("Valor em R$ a abastecer: ", integer=False)

      try:
        litros_abastecidos = bomba.abastecer_por_valor(valor)
        print(f'Abastecimento completo. Você colocou {litros_abastecidos:.2f} litros de {bomba.tipo_combustivel}')
      except Exception as ex:
        print(ex)

    # Ajustar valor do combustível
    elif opcao == 3:
      valor = tela.ler_numero("Novo valor em R$: ", integer=False)

      try:
        bomba.alterar_valor(valor)
        print('Alteração de valor realizada com sucesso')
      except Exception as ex:
        print(ex)

    # Ajustar quantidade de combustível
    elif opcao == 4:
      quantidade = tela.ler_numero(f'Nova quantidade de {bomba.tipo_combustivel}: ', integer=False)

      try:
        bomba.alterar_quantidade_combustivel(quantidade)
        print('Alteração de quantidade realizada com sucesso')
      except Exception as ex:
        print(ex)

    else:
      print('Opção inválida.')
    
    input('Pressione enter para continuar')


if __name__ == '__main__':
  main()
