from os import system


def menu(tipo_combustivel, valor_litro, quantidade_combustivel):
  system('clear')

  print('##########**********###########')
  print(f'Bomba de combustível: {tipo_combustivel}')
  print(f'Preço por litro: R$ {valor_litro}')
  print(f'Quantidade disponível: {quantidade_combustivel:.2f}L')
  print('##########**********###########\n')

  print('1 - Abastecer veículo por litro')
  print('2 - Abastecer veículo por valor')
  print('3 - Ajustar valor do combustível')
  print('4 - Ajustar quantidade de combustível')
  print('0 - Sair')


def ler_numero(texto, integer=True):
  valor = input(texto)

  try:
    valor = int(valor) if integer else float(valor)
    ok = True
  except:
    ok = False

  return valor if ok else None
