class BombaCombustivel:
  def __init__(self, valor_litro, quantidade_combustivel, tipo_combustivel='gasolina'):
    self.__tipo_combustivel = tipo_combustivel
    self.__valor_litro = valor_litro
    self.__quantidade_combustivel = quantidade_combustivel


  @property
  def tipo_combustivel(self):
    return self.__tipo_combustivel
  
  @property
  def valor_litro(self):
    return self.__valor_litro

  @property
  def quantidade_combustivel(self):
    return self.__quantidade_combustivel


  def abastecer_por_valor(self, valor_abastecido):
    qtd_abastecida = valor_abastecido / self.__valor_litro

    if qtd_abastecida > self.__quantidade_combustivel:
      raise Exception('Não há combustível suficiente para abastecer esse valor')

    self.__quantidade_combustivel -= qtd_abastecida

    return qtd_abastecida


  def abastecer_por_litro(self, litros):
    if litros > self.__quantidade_combustivel:
      raise Exception('Não há combustível suficiente para abastecer esse valor')

    self.__quantidade_combustivel -= litros

    return litros * self.__valor_litro


  def alterar_valor(self, valor):
    if valor < 0:
      raise Exception('Valor deve ser maior ou igual a zero')

    self.__valor_litro = valor


  def alterar_combustivel(self, tipo):
    self.__tipo_combustivel = tipo


  def alterar_quantidade_combustivel(self, quantidade):
    if quantidade < 0:
      raise Exception('Valor deve ser maior ou igual a zero')

    self.__quantidade_combustivel = quantidade
