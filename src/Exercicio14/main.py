'''
Faça um programa que solicite dois valores e imprima todos os pares entre o menor valor e o maior valor. Caso
os números digitados sejam iguais, enviar mensagem ao usuário e imprimir os pares de 1 a 10 como exemplo.
'''


def encerrar_programa_com_erro(mensagem):
    print(f'{mensagem} Fim do programa.')
    exit(-1)


def main():
    valores = input(
        'Informe dois valores separados por vírgula. Ex (10,100): ').split(',')
    if len(valores) != 2:
        encerrar_programa_com_erro(
            'Informe dois valores separados por vírgula')

    try:
        inicio = int(valores[0].strip())
        fim = int(valores[1].strip())

        if inicio == fim:
            inicio = 1
            fim = 10
    except:
        encerrar_programa_com_erro('Valores no formato inválido.')

    print('Pares encontrados: ')
    for i in range(inicio, fim+1):
        if i % 2 == 0:
            print(i)


if __name__ == '__main__':
    main()
