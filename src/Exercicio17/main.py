'''
Desenvolva um gerador de tabuada, capaz de gerar a tabuada de qualquer número inteiro entre 1 a 10. O
usuário deve informar de qual número ele deseja ver a tabuada. A saída deve ser conforme o exemplo abaixo:
Tabuada de 5:
5 X 1 = 5
5 X 2 = 10
...
5 X 10 = 50
'''


def gerador_tabuada(numero):
    print(f'Tabuada de {numero}: ')
    for n in range(10):
        n += 1
        print(f'\t{numero}x{n} = {numero*n}')


def encerrar_programa_com_erro():
    print('Número inválido. Fim do programa.')
    exit(-1)


def main():
    numero = input('Informe um número entre 1 e 10: ')

    try:
        numero = int(numero)
    except:
        encerrar_programa_com_erro()

    if numero < 1 or numero > 10:
        encerrar_programa_com_erro()

    gerador_tabuada(numero)


if __name__ == '__main__':
    main()
