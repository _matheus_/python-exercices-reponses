'''
Faça um Programa que leia as idades e alturas de 10 alunos e determine quantos
alunos com mais de 13 anos possuem altura inferior à média de altura desses alunos.
'''

from lib import Aluno, ler_numero

def main():
  NUM_ALUNOS = 3
  alunos = []

  for i in range(NUM_ALUNOS):
    print(f'Aluno {i + 1}:')

    idade = ler_numero("Idade: ")
    if idade is None:
      print(f"Idade inválida. Fim do programa.")
      exit(-1)

    altura = ler_numero("Altura: ", integer=False)
    if altura is None:
      print(f"Altura inválida. Fim do programa.")
      exit(-2)

    aluno = Aluno(idade=idade, altura=altura)
    alunos.append(aluno)

  media_altura = sum([aluno.altura for aluno in alunos]) / NUM_ALUNOS
  qtd_alunos_altura_media = sum([1 if aluno.idade >= 13 and aluno.altura < media_altura else 0 for aluno in alunos])

  print(f'Alunos com mais de 13 anos e altura inferior à média ({media_altura}): {qtd_alunos_altura_media}')


if __name__ == '__main__':
  main()
