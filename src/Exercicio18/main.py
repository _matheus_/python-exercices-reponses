'''
Implemente uma classe chamada Carro de acordo com as seguintes regras:
    a. um veículo tem um certo consumo de combustível (medidos em km / litro) e uma certa quantidade de
combustível no tanque;
    b. o consumo é especificado no construtor e o nível de combustível inicial é 0;
    c. forneça um método andar( ) que simula o ato de dirigir o veículo por uma certa distância, reduzindo o
nível de combustível no tanque de gasolina;
    d. forneça um método obterGasolina( ), que retorna o nível atual de combustível;
    e. forneça um método adicionarGasolina( ), para abastecer o tanque.
'''
from lib import Carro


def main():
    carro = Carro(4/1)
    carro.adicionar_gasolina(10)
    print(f'Quantidade de gasolina no tanque: {carro.obter_gasolina()}')
    print('Andando por 3km')
    carro.andar(3)
    print(f'Tanque atual: {carro.obter_gasolina()}')


if __name__ == '__main__':
    main()
