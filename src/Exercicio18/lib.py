class Carro:
    def __init__(self, consumo_por_km):
        self._combustivel_tanque = 0
        self._consumo_por_km = consumo_por_km

    def andar(self, distancia):
        self._combustivel_tanque -= distancia/self._consumo_por_km

    def obter_gasolina(self):
        return self._combustivel_tanque

    def adicionar_gasolina(self, quantidade):
        self._combustivel_tanque += quantidade
