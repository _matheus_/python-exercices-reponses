'''
Faça um programa que peça dois números (base e expoente) e calcule e mostre o primeiro número elevado ao
segundo número. Não utilize a função de potência nativa da linguagem.
'''


def main():
    try:
        base = int(input('Informe a base: '))
        expoente = int(input('Informe o expoente: '))
    except:
        print('Entrada inválida. Fim do programa.')
        exit(-1)

    resultado_final = base

    for _ in range(expoente-1):
        resultado_final *= base

    print(f'Resultado final {resultado_final}')


if __name__ == '__main__':
    main()
